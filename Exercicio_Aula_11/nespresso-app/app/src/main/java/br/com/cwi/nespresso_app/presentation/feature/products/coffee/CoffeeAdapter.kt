package br.com.cwi.nespresso_app.presentation.feature.products.coffee

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.*
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.domain.entity.Category
import br.com.cwi.nespresso_app.domain.entity.Coffee
import br.com.cwi.nespresso_app.domain.entity.ItemType
import br.com.cwi.nespresso_app.domain.entity.Type
import br.com.cwi.nespresso_app.presentation.feature.products.viewholder.CoffeeViewHolder
import br.com.cwi.nespresso_app.presentation.feature.products.viewholder.CategoryCoffeViewHolder

class CoffeeAdapter(
    private val list: List<Type>,
    private val onFavoriteClick: (Coffee) -> Unit,
    private val onCoffeeClick: (Coffee) -> Unit
) : Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == ItemType.CATEGORY.value) {
            val view = inflateView(R.layout.item_coffe_category, parent)
            CategoryCoffeViewHolder(view)

        } else {
            val view = inflateView(R.layout.item_coffee, parent)
            CoffeeViewHolder(view, onFavoriteClick, onCoffeeClick)
        }
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = list[position]

        if (item.type == ItemType.CATEGORY) {
            item as Category
            (viewHolder as CategoryCoffeViewHolder).bind(item)

        } else {
            item as Coffee
            (viewHolder as CoffeeViewHolder).bind(item)
        }
    }

    override fun getItemCount() = list.size

    override fun getItemViewType(position: Int) = list[position].type.value

    private fun inflateView(layout: Int, parent: ViewGroup) = LayoutInflater.from(parent.context)
        .inflate(layout, parent, false)
}