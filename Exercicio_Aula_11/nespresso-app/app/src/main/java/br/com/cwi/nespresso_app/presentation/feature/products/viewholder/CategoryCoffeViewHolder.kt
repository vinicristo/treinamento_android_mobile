package br.com.cwi.nespresso_app.presentation.feature.products.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.databinding.ItemCoffeCategoryBinding
import br.com.cwi.nespresso_app.domain.entity.Category

class CategoryCoffeViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    private val tvCategory = ItemCoffeCategoryBinding.bind(item).tvCapTitleCategory

    fun bind(item: Category) {
        tvCategory.text = item.category
    }
}