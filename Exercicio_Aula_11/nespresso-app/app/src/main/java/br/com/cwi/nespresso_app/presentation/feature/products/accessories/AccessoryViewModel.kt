package br.com.cwi.nespresso_app.presentation.feature.products.accessories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.cwi.nespresso_app.data.database.mapper.toEntity
import br.com.cwi.nespresso_app.domain.entity.Accessory
import br.com.cwi.nespresso_app.domain.entity.Category
import br.com.cwi.nespresso_app.domain.entity.Type
import br.com.cwi.nespresso_app.domain.repository.AccessoryLocalRepository
import br.com.cwi.nespresso_app.domain.repository.CoffeeRepository
import br.com.cwi.nespresso_app.presentation.base.BaseViewModel

class AccessoryViewModel(
    private val accessoryRepository: CoffeeRepository,
    private val accessoryLocalRepository: AccessoryLocalRepository
) : BaseViewModel() {

    private val _accessories = MutableLiveData<List<Type>>()
    val accessories: LiveData<List<Type>> = _accessories

    var accessoryId: Int? = null
    var accessoryTitle: Int? = null
    var accessoryImage: Int? = null
    var accessoryPrice: Int? = null

    fun fetchAccessories() {
        launch {
            val categoryList = accessoryRepository.getAccessories()
            _accessories.postValue(getAccessoriesType(categoryList))
        }
    }

    private fun getAccessoriesType(categoryList: List<Category>): List<Type> {
        val accessoryList = mutableListOf<Type>()

        val favoriteList = accessoryLocalRepository.getAll()

        categoryList.forEach { category ->
            accessoryList.add(category)
            favoriteList?.takeIf { it.isNotEmpty() }?.let { favoriteList ->
                setIsAccessoryFavorite(favoriteList.map { it.id }, category.products.map { it as Accessory })
            }
            accessoryList.addAll(category.products)
        }
        return accessoryList
    }

    private fun setIsAccessoryFavorite(favoriteIdList: List<Int>, accessoryList: List<Accessory>) {
        favoriteIdList.forEach { id ->
            accessoryList.forEach { it.isFavorite = it.id == id }
        }
    }

    fun setFavorite(accessory: Accessory) {
        val accessoryEntity = accessory.toEntity()
        if (accessory.isFavorite) accessoryLocalRepository.add(accessoryEntity)
        else accessoryLocalRepository.remove(accessoryEntity)
    }

}