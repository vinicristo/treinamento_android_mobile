package br.com.cwi.nespresso_app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.cwi.nespresso_app.databinding.FragmentAccessoryDetailBinding
import com.bumptech.glide.Glide

const val EXTRA_ACCESSORY_ID = "EXTRA_ACCESSORY_ID"
const val EXTRA_ACCESSORY_TITLE = "EXTRA_ACCESSORY_TITLE"
const val EXTRA_ACCESSORY_IMAGE = "EXTRA_ACCESSORY_IMAGE"
const val EXTRA_ACCESSORY_DESCRIPTION = "EXTRA_ACCESSORY_DESCRIPTION"
const val EXTRA_ACCESSORY_PRICE = "EXTRA_ACCESSORY_PRICE"

class AccessoryDetailFragment : Fragment() {

    private lateinit var binding: FragmentAccessoryDetailBinding

    private val accessoryTitle by lazy {
        arguments?.getString(EXTRA_ACCESSORY_TITLE)
    }
    private val accessoryImage by lazy {
        arguments?.getString(EXTRA_ACCESSORY_IMAGE)
    }
    private val accessoryPrice by lazy {
        arguments?.getDouble(EXTRA_ACCESSORY_PRICE)
    }
    private val accessoryDescription by lazy {
        arguments?.getString(EXTRA_ACCESSORY_DESCRIPTION)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAccessoryDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let { Glide.with(it).load(accessoryImage.toString()).into(binding.tvAccessoryDetailImage) }
        binding.tvAccessoryDetailTitle.text = accessoryTitle.toString()
        binding.tvAccessoryDetailPrice.text = accessoryPrice.toString()
        binding.tvAccessoryDetailDescription.text = accessoryDescription.toString()
    }

}