package br.com.cwi.nespresso_app.data.database

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.cwi.nespresso_app.data.database.dao.AccessoryDao
import br.com.cwi.nespresso_app.data.database.entity.AccessoryEntity

@Database(entities = [AccessoryEntity::class], version = 1)
abstract class AppAccessoryDatabase : RoomDatabase() {

    abstract fun getAccessoryDao(): AccessoryDao

    companion object {
        private const val DATABASE_NAME = "nespresso-db-accessory"
        fun create(application: Application): AppAccessoryDatabase {
            return Room.databaseBuilder(
                application,
                AppAccessoryDatabase::class.java,
                DATABASE_NAME
            )
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}