package br.com.cwi.nespresso_app.presentation.feature.products.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.databinding.ItemAccessoryCategoryBinding
import br.com.cwi.nespresso_app.domain.entity.Category

class CategoryAccessoryViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    private val tvCategory = ItemAccessoryCategoryBinding.bind(item).tvAccsTitleCategory

    fun bind(item: Category) {
        tvCategory.text = item.category
    }
}