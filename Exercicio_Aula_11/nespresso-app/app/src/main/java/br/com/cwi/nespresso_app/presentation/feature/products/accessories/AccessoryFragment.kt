package br.com.cwi.nespresso_app.presentation.feature.products.accessories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import br.com.cwi.nespresso_app.*
import br.com.cwi.nespresso_app.databinding.FragmentAccessoryBinding
import br.com.cwi.nespresso_app.domain.entity.Type
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AccessoryFragment : Fragment() {

    private lateinit var binding: FragmentAccessoryBinding

    private val viewModel: AccessoryViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAccessoryBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel.accessories.observe(viewLifecycleOwner) { list ->
            setUpAccessoryRecyclerView(list)
        }
        viewModel.fetchAccessories()
    }

    private fun setUpAccessoryRecyclerView(list: List<Type>) {
        binding.rvAccs.apply {
            addItemDecoration(
                DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
            )
            adapter = AccessoryAdapter(list, onFavoriteClick = {
                viewModel.setFavorite(it)
            }, onAccessoryClick = {
                navigateToAccessoryDetail(it.id, it.name, it.urlImage, it.unitPrice, it.description)
            })
        }
    }

    private fun navigateToAccessoryDetail(
        id: Int,
        title: String,
        image: String?,
        price: Double,
        description: String?
    ) {
        findNavController().navigate(
            R.id.accessoryDetailFragment,
            bundleOf(
                Pair(EXTRA_ACCESSORY_ID, id),
                Pair(EXTRA_ACCESSORY_TITLE, title),
                Pair(EXTRA_ACCESSORY_IMAGE, image),
                Pair(EXTRA_ACCESSORY_PRICE, price),
                Pair(EXTRA_ACCESSORY_DESCRIPTION, description)
            )
        )
    }
}