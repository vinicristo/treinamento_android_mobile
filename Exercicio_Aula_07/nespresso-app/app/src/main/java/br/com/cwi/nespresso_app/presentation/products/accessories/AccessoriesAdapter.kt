package br.com.cwi.nespresso_app.presentation.products.accessories

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.domain.entity.Accessories
import br.com.cwi.nespresso_app.domain.entity.CategoryAccessories
import br.com.cwi.nespresso_app.domain.entity.ItemType
import br.com.cwi.nespresso_app.domain.entity.Type
import br.com.cwi.nespresso_app.presentation.products.accessories.viewholder.AccessoriesViewHolder
import br.com.cwi.nespresso_app.presentation.products.accessories.viewholder.CategoryAccessoriesViewHolder

class AccessoriesAdapter(private val context: Context, private val list: List<Type>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ItemType.CATEGORY.value) {
            val view = inflateView(R.layout.item_acc_category, parent)
            CategoryAccessoriesViewHolder(view)

        } else {
            val view = inflateView(R.layout.item_accessories, parent)
            AccessoriesViewHolder(view)

        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]

        if (item.type == ItemType.CATEGORY) {
            item as CategoryAccessories
            (viewHolder as CategoryAccessoriesViewHolder).bind(item)

        } else {
            item as Accessories
            (viewHolder as AccessoriesViewHolder).bind(context, item)
        }
    }

    override fun getItemCount() = list.size

    override fun getItemViewType(position: Int) = list[position].type.value

    private fun inflateView(layout: Int, parent: ViewGroup) = LayoutInflater.from(parent.context)
        .inflate(layout, parent, false)
}