package br.com.cwi.nespresso_app.domain.entity

class CategoryCoffe(
    val category: String,
    val coffees: List<Product>,
): Type(ItemType.CATEGORY)