package br.com.cwi.nespresso_app.data.mapper

import br.com.cwi.nespresso_app.data.entity.AccessoriesResponse
import br.com.cwi.nespresso_app.domain.entity.Accessories

class AccessoriesMapper : DomainMapper<AccessoriesResponse, Accessories> {

    override fun toDomain(from: AccessoriesResponse) = Accessories(
        id = from.id,
        name = from.name,
        unitPrice = from.price,
        urlImage = from.urlImage,

    )


    override fun toDomain(from: List<AccessoriesResponse>) = from.map {
        toDomain(it)
    }
}