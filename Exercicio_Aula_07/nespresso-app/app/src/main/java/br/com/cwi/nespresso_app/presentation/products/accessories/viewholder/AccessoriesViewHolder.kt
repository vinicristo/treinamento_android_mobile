package br.com.cwi.nespresso_app.presentation.products.accessories.viewholder

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.databinding.ItemAccessoriesBinding
import br.com.cwi.nespresso_app.domain.entity.Accessories
import br.com.cwi.nespresso_app.presentation.extension.toMoneyFormat
import com.bumptech.glide.Glide

class AccessoriesViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    private val tvTitle = ItemAccessoriesBinding.bind(item).tvAccessorieTitle
    private val ivImage = ItemAccessoriesBinding.bind(item).ivAccessorieImage
    private val tvPrice = ItemAccessoriesBinding.bind(item).tvAccessoriePrice
    private val ivFavorite = ItemAccessoriesBinding.bind(item).ivFavorite

    fun bind(context: Context, item: Accessories) {
        tvTitle.text = item.name
        tvPrice.text = item.unitPrice.toMoneyFormat()

        Glide.with(context)
            .load(item.urlImage)
            .into(ivImage)
    }
}