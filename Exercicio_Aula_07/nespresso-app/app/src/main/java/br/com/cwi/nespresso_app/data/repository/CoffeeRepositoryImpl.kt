package br.com.cwi.nespresso_app.data.repository

import br.com.cwi.nespresso_app.data.mapper.CategoryAccessoriesMapper
import br.com.cwi.nespresso_app.data.mapper.CategoryCoffeMapper
import br.com.cwi.nespresso_app.data.mapper.MachineMapper
import br.com.cwi.nespresso_app.data.network.NespressoApi
import br.com.cwi.nespresso_app.data.network.RetrofitConfig
import br.com.cwi.nespresso_app.domain.entity.CategoryAccessories
import br.com.cwi.nespresso_app.domain.entity.CategoryCoffe
import br.com.cwi.nespresso_app.domain.entity.Machine
import br.com.cwi.nespresso_app.domain.repository.CoffeeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CoffeeRepositoryImpl(
    private val categoryCoffeMapper: CategoryCoffeMapper,
    private val machineMapper: MachineMapper,
    private val categoryAccessoriesMapper: CategoryAccessoriesMapper
) : CoffeeRepository {

    private val api: NespressoApi = RetrofitConfig.service

    override suspend fun getCoffees(): List<CategoryCoffe> {
        return withContext(Dispatchers.IO) {
            categoryCoffeMapper.toDomain(api.getCoffees())
        }
    }

    override suspend fun getMachines(): List<Machine> {
        return withContext(Dispatchers.IO) {
            machineMapper.toDomain(api.getMachines())
        }
    }

    override suspend fun getAccessories(): List<CategoryAccessories> {
        return withContext(Dispatchers.IO) {
            categoryAccessoriesMapper.toDomain(api.getAccessories())
        }
    }


}