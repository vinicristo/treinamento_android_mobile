package br.com.cwi.nespresso_app.domain.repository

import br.com.cwi.nespresso_app.domain.entity.CategoryAccessories
import br.com.cwi.nespresso_app.domain.entity.CategoryCoffe
import br.com.cwi.nespresso_app.domain.entity.Machine

interface CoffeeRepository {
    suspend fun getCoffees(): List<CategoryCoffe>
    suspend fun getMachines(): List<Machine>
    suspend fun getAccessories(): List<CategoryAccessories>
}