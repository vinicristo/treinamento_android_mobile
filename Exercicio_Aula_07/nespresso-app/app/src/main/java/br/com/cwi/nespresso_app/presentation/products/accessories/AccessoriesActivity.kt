package br.com.cwi.nespresso_app.presentation.products.accessories

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.data.mapper.CategoryAccessoriesMapper
import br.com.cwi.nespresso_app.data.mapper.CategoryCoffeMapper
import br.com.cwi.nespresso_app.data.mapper.MachineMapper
import br.com.cwi.nespresso_app.data.repository.CoffeeRepositoryImpl
import br.com.cwi.nespresso_app.databinding.ActivityAccessoriesBinding
import br.com.cwi.nespresso_app.domain.entity.Type
import br.com.cwi.nespresso_app.domain.repository.CoffeeRepository
import br.com.cwi.nespresso_app.presentation.base.BaseBottomNavigation
import br.com.cwi.nespresso_app.presentation.products.coffee.CapsulesAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AccessoriesActivity : BaseBottomNavigation() {

    private lateinit var binding: ActivityAccessoriesBinding

    private val repository: CoffeeRepository =
        CoffeeRepositoryImpl(CategoryCoffeMapper(), MachineMapper(), CategoryAccessoriesMapper())

    override val currentTab: Int = R.id.products_menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccessoriesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setUpAccessoriesList()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun getBottomNavigation() = binding.bottomNavigation

    private fun setUpAccessoriesList() {

        CoroutineScope(Dispatchers.Main).launch {
            repository.getAccessories().let { categoryList ->
                val recyclerView = binding.rvAccs

                recyclerView.addItemDecoration(
                    DividerItemDecoration(this@AccessoriesActivity, DividerItemDecoration.VERTICAL)
                )

                val accessoriesList = mutableListOf<Type>()

                categoryList.forEach {
                    accessoriesList.add(it)
                    accessoriesList.addAll(it.accessories)
                }

                recyclerView.adapter = AccessoriesAdapter(this@AccessoriesActivity, accessoriesList)
            }
        }


    }
}