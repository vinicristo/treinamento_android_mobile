package br.com.cwi.nespresso_app.data.entity

sealed class AccessorieType(
    val type: Int
)