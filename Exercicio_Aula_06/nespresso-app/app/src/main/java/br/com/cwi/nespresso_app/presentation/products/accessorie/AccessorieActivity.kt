package br.com.cwi.nespresso_app.presentation.products.accessorie

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.data.entity.AccessorieType
import br.com.cwi.nespresso_app.data.entity.CapsuleType
import br.com.cwi.nespresso_app.data.repository.CoffeeRepository
import br.com.cwi.nespresso_app.databinding.ActivityAccessorieBinding
import br.com.cwi.nespresso_app.databinding.ActivityCoffeeBinding
import br.com.cwi.nespresso_app.presentation.base.BaseBottomNavigation
import br.com.cwi.nespresso_app.presentation.products.coffee.CapsulesAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AccessorieActivity: BaseBottomNavigation() {

    private lateinit var binding: ActivityAccessorieBinding

    private val repository = CoffeeRepository()

    override val currentTab: Int = R.id.products_menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccessorieBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setUpCapsList()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun getBottomNavigation() = binding.bottomNavigation

    private fun setUpCapsList() {

        CoroutineScope(Dispatchers.Main).launch {
            repository.getAccessories().let { categoryList ->
                val recyclerView = binding.rvAccs

                recyclerView.addItemDecoration(
                    DividerItemDecoration(this@AccessorieActivity, DividerItemDecoration.VERTICAL)
                )

                val accessorieList = mutableListOf<AccessorieType>()

                categoryList.forEach {
                    accessorieList.add(it)
                    accessorieList.addAll(it.itens)
                }

                recyclerView.adapter = AccessorieAdapter(this@AccessorieActivity, accessorieList)
            }
        }


    }
}