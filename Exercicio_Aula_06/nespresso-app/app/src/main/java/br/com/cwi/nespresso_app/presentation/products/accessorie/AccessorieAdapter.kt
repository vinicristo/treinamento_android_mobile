package br.com.cwi.nespresso_app.presentation.products.accessorie

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.data.entity.*
import br.com.cwi.nespresso_app.databinding.ItemAccCategoryBinding
import br.com.cwi.nespresso_app.databinding.ItemAccessorieBinding
import br.com.cwi.nespresso_app.presentation.extension.toMoneyFormat
import com.bumptech.glide.Glide

const val VIEW_TYPE_CATEGORY = 0

class AccessorieAdapter(val context: Context, private val list: List<AccessorieType>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_CATEGORY) {

            val view = inflateView(R.layout.item_acc_category, parent)
            CategoryViewHolder(view)

        } else {

            val view = inflateView(R.layout.item_accessorie, parent)
            AccessorieViewHolder(view)

        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]

        if (item.type == VIEW_TYPE_CATEGORY) {

            item as CategoryAccessorieResponse
            (viewHolder as CategoryViewHolder).bind(item)

        } else {

            item as AccessorieResponse
            (viewHolder as AccessorieViewHolder).bind(context, item)

        }
    }

    override fun getItemCount() = list.size

    override fun getItemViewType(position: Int) = list[position].type

    private fun inflateView(layout: Int, parent: ViewGroup) = LayoutInflater.from(parent.context)
        .inflate(layout, parent, false)


    class CategoryViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val tvCategory = ItemAccCategoryBinding.bind(item).tvAccTitleCategory

        fun bind(item: CategoryAccessorieResponse) {
            tvCategory.text = item.category
        }
    }

    class AccessorieViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val tvTitle = ItemAccessorieBinding.bind(item).tvAccessorieTitle
        private val tvPrice = ItemAccessorieBinding.bind(item).tvAccessoriePrice
        private val ivImage = ItemAccessorieBinding.bind(item).ivAccessorieImage
        private val ivFavorite = ItemAccessorieBinding.bind(item).ivFavorite

        fun bind(context: Context, item: AccessorieResponse) {
            tvTitle.text = item.name
            tvPrice.text = item.price.toMoneyFormat()

            Glide.with(context)
                .load(item.urlImage)
                .into(ivImage)
        }
    }

}