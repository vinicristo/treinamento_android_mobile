package br.com.cwi.nespresso_app.data.entity

import com.squareup.moshi.Json

class CategoryAccessorieResponse(
    @Json(name = "categoria") val category: String,
    @Json(name = "itens") val itens: List<AccessorieResponse>
) : AccessorieType(type = 0)