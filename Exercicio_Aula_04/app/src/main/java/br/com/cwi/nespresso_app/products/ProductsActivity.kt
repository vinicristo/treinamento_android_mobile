package br.com.cwi.nespresso_app.products

import android.content.Intent
import android.os.Bundle
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.databinding.ActivityProductsBinding
import br.com.cwi.nespresso_app.products.accessorie.AccessorieActivity
import br.com.cwi.nespresso_app.base.BaseBottomNavigation
import br.com.cwi.nespresso_app.products.coffee.CoffeeActivity
import br.com.cwi.nespresso_app.products.machine.MachineActivity

class ProductsActivity : BaseBottomNavigation() {

    private lateinit var binding: ActivityProductsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpProductsActions()
        bottomNavigationControl(binding)
    }

    override fun onResume() {
        super.onResume()

        binding.bottomNavigation.selectedItemId = R.id.products_menu
    }

    override fun onPause() {
        super.onPause()
        overridePendingTransition(0, 0)
    }

    private fun setUpProductsActions() {
        binding.contentCoffees.root.setOnClickListener {
            val intent = Intent(this, CoffeeActivity::class.java)
                .putExtra("EXTRA_MESSAGE", "Bem vindo a tela dos cafés!")

            startActivity(intent)
        }

        binding.contentMachines.root.setOnClickListener {
            val intent = Intent(this, MachineActivity::class.java)
                .putExtra("EXTRA_MESSAGE", "Bem vindo a tela das máquinas!")

            startActivity(intent)
        }

        binding.contentAccessories.root.setOnClickListener {
            val intent = Intent(this, AccessorieActivity::class.java)
                .putExtra("EXTRA_MESSAGE", "Bem vindo a tela dos acessórios!")

            startActivity(intent)
        }
    }


}