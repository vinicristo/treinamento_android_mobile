package br.com.cwi.nespresso_app.products.coffee

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.bag.BagActivity
import br.com.cwi.nespresso_app.databinding.ActivityCoffeeBinding
import br.com.cwi.nespresso_app.favorites.FavoritesActivity
import br.com.cwi.nespresso_app.base.BaseBottomNavigation

class CoffeeActivity : BaseBottomNavigation() {

    private lateinit var binding: ActivityCoffeeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCoffeeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Exemplo de passagem de paremetro entre Activities
        val mensagem : String? = intent.getStringExtra("EXTRA_MESSAGE")

        AlertDialog.Builder(this)
            .setMessage("OPA")
            .setTitle(mensagem)
            .show()

        setUpBottomNavigation()
    }

    override fun onResume() {
        super.onResume()

        binding.bottomNavigation.selectedItemId = R.id.products_menu
    }

    override fun onPause() {
        super.onPause()
        overridePendingTransition(0, 0)
    }

    private fun setUpBottomNavigation() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener {

            when (it.itemId) {
                R.id.favorites_menu -> {
                    startActivity(Intent(this, FavoritesActivity::class.java))
                }
                R.id.bag_menu -> {
                    startActivity(Intent(this, BagActivity::class.java))
                }
            }
            return@setOnNavigationItemSelectedListener true
        }

    }
}