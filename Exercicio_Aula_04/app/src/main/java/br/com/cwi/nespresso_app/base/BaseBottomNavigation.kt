package br.com.cwi.nespresso_app.base

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.bag.BagActivity
import br.com.cwi.nespresso_app.databinding.ActivityBagBinding
import br.com.cwi.nespresso_app.databinding.ActivityFavoritesBinding
import br.com.cwi.nespresso_app.databinding.ActivityProductsBinding
import br.com.cwi.nespresso_app.favorites.FavoritesActivity
import br.com.cwi.nespresso_app.products.ProductsActivity

abstract class BaseBottomNavigation : AppCompatActivity() {

//    abstract fun bottomNavigationView() : BottomNavigationView

    fun bottomNavigationControl(bindingProducts: ActivityProductsBinding) {
        bindingProducts.bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.favorites_menu -> {
                    startActivity(Intent(this, FavoritesActivity::class.java))
                }
                R.id.bag_menu -> {
                    startActivity(Intent(this, BagActivity::class.java))
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    fun bottomNavigationControl(bindingBag: ActivityBagBinding) {
        bindingBag.bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.products_menu -> {
                    startActivity(Intent(this, ProductsActivity::class.java))
                }
                R.id.favorites_menu -> {
                    startActivity(Intent(this, FavoritesActivity::class.java))
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    fun bottomNavigationControl(bindingFavorite: ActivityFavoritesBinding) {
        bindingFavorite.bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.products_menu -> {
                    startActivity(Intent(this, ProductsActivity::class.java))
                }
                R.id.bag_menu -> {
                    startActivity(Intent(this, BagActivity::class.java))
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

}