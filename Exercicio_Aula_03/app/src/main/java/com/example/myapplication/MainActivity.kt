package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    private lateinit var etAge: AppCompatEditText
    private lateinit var etHeight: AppCompatEditText
    private lateinit var etWeight: AppCompatEditText
    private lateinit var tvResult: AppCompatTextView
    private lateinit var bCalculate: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etAge = findViewById(R.id.et_age)
        etHeight = findViewById(R.id.et_height)
        etWeight = findViewById(R.id.et_weight)
        tvResult = findViewById(R.id.tv_result)
        bCalculate = findViewById(R.id.b_calcular)

        bCalculate.setOnClickListener {
            val dec = DecimalFormat("#,###.00")
            val height = (etHeight.text.toString().toDouble()) / 100
            val weight = etWeight.text.toString().toDouble()
            val age = etAge.text.toString().toInt()
            val result = (weight / (height * height))
            val resultFormat = "Seu IMC é " + dec.format(result)

            tvResult.text = if (age in 20..65) {
                when (result) {
                    in 0.0..18.4 -> resultFormat + ". Você está abaixo do peso."
                    in 18.5..24.9 -> resultFormat + ". Você está com o peso normal."
                    else -> resultFormat + ". Você está acima do peso."
                }
            } else if(age > 65) {
                when (result) {
                    in 0.0..21.9 -> resultFormat+ ". Você está abaixo do peso."
                    in 22.0..26.9 -> resultFormat + ". Você está com o peso normal."
                    else -> resultFormat + ". Você está acima do peso."
                }
            } else {
                    "Você não tem idade para calcular seu IMC!"
            }
            tvResult.visibility = View.VISIBLE
        }

    }
}