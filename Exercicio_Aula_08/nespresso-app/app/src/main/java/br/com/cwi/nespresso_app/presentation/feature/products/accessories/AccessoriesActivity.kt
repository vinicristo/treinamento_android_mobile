package br.com.cwi.nespresso_app.presentation.feature.products.accessories

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.databinding.ActivityAccessoriesBinding
import br.com.cwi.nespresso_app.presentation.base.BaseBottomNavigation
import br.com.cwi.nespresso_app.presentation.extension.visibleOrGone

class AccessoriesActivity : BaseBottomNavigation() {

    private lateinit var binding: ActivityAccessoriesBinding

    private val viewModel = AccessoriesViewModel()

    override val currentTab: Int = R.id.products_menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccessoriesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setUpViewModel()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun getBottomNavigation() = binding.bottomNavigation

    private fun setUpViewModel() {

        viewModel.loading.observe(this@AccessoriesActivity) { isLoading ->
            binding.viewLoading.root.visibleOrGone(isLoading)
        }

        viewModel.error.observe(this@AccessoriesActivity) { hasError ->
            binding.viewError.root.visibleOrGone(hasError)
        }

        viewModel.accessories.observe(this@AccessoriesActivity) { list ->
            val recyclerView = binding.rvAccs

            recyclerView.addItemDecoration(
                DividerItemDecoration(this@AccessoriesActivity, DividerItemDecoration.VERTICAL)
            )

            recyclerView.adapter = AccessoriesAdapter(this@AccessoriesActivity, list)
        }

        viewModel.fetchAccessories()


    }
}