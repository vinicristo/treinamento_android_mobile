package br.com.cwi.nespresso_app.data.mapper

import br.com.cwi.nespresso_app.data.entity.CategoryAccessoriesResponse
import br.com.cwi.nespresso_app.domain.entity.CategoryAccessories

class CategoryAccessoriesMapper: DomainMapper<CategoryAccessoriesResponse, CategoryAccessories> {
    override fun toDomain(from: CategoryAccessoriesResponse) = CategoryAccessories(
        category = from.category,
        accessories = AccessoriesMapper().toDomain(from.accessories)
    )

    override fun toDomain(from: List<CategoryAccessoriesResponse>) = from.map {
        toDomain(it)
    }

}