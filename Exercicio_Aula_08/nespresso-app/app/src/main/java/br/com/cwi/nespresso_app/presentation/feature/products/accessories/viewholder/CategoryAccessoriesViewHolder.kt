package br.com.cwi.nespresso_app.presentation.products.accessories.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.databinding.ItemAccCategoryBinding
import br.com.cwi.nespresso_app.domain.entity.CategoryAccessories

class CategoryAccessoriesViewHolder (item: View) : RecyclerView.ViewHolder(item) {
    private val tvCategoryAccessories = ItemAccCategoryBinding.bind(item).tvAccTitleCategory

    fun bind(item: CategoryAccessories) {
        tvCategoryAccessories.text = item.category
    }
}