package br.com.cwi.nespresso_app.presentation.feature.products.machine.viewholder

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.databinding.ItemMachineBinding
import br.com.cwi.nespresso_app.domain.entity.Coffee
import br.com.cwi.nespresso_app.domain.entity.Machine
import br.com.cwi.nespresso_app.presentation.extension.toMoneyFormat
import com.bumptech.glide.Glide

class MachineViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    val tvName = ItemMachineBinding.bind(item).tvMachineName
    val tvPrice = ItemMachineBinding.bind(item).tvMachinePrice
    val ivMachine = ItemMachineBinding.bind(item).ivMachinePhoto
}