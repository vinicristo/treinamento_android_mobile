package br.com.cwi.nespresso_app.domain.entity

class CategoryAccessories(
    val category: String,
    val accessories: List<Product>,
): Type(ItemType.CATEGORY)