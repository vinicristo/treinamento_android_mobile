package br.com.cwi.nespresso_app.presentation.feature.products.coffee

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.cwi.nespresso_app.data.mapper.CategoryAccessoriesMapper
import br.com.cwi.nespresso_app.data.mapper.CategoryCoffeMapper
import br.com.cwi.nespresso_app.data.mapper.MachineMapper
import br.com.cwi.nespresso_app.data.repository.CoffeeRepositoryImpl
import br.com.cwi.nespresso_app.domain.entity.CategoryCoffe
import br.com.cwi.nespresso_app.domain.entity.Type
import br.com.cwi.nespresso_app.domain.repository.CoffeeRepository
import br.com.cwi.nespresso_app.presentation.base.BaseViewModel

class CoffeeViewModel : BaseViewModel() {

    private val _coffees = MutableLiveData<List<Type>>()
    val coffees: LiveData<List<Type>> = _coffees

    private val repository: CoffeeRepository = CoffeeRepositoryImpl(
        CategoryCoffeMapper(), MachineMapper(), CategoryAccessoriesMapper()
    )

    fun fetchCoffees() {
        launch {
            val categoryList = repository.getCoffees()
            _coffees.postValue(getCoffeesType(categoryList))
        }
    }

    private fun getCoffeesType(categoryCoffeList: List<CategoryCoffe>): List<Type> {
        val coffeeList = mutableListOf<Type>()

        categoryCoffeList.forEach {
            coffeeList.add(it)
            coffeeList.addAll(it.coffees)
        }
        return coffeeList
    }

}