package br.com.cwi.nespresso_app.presentation.feature.products.accessories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.cwi.nespresso_app.data.mapper.CategoryAccessoriesMapper
import br.com.cwi.nespresso_app.data.mapper.CategoryCoffeMapper
import br.com.cwi.nespresso_app.data.mapper.MachineMapper
import br.com.cwi.nespresso_app.data.repository.CoffeeRepositoryImpl
import br.com.cwi.nespresso_app.domain.entity.CategoryAccessories
import br.com.cwi.nespresso_app.domain.entity.CategoryCoffe
import br.com.cwi.nespresso_app.domain.entity.Type
import br.com.cwi.nespresso_app.domain.repository.CoffeeRepository
import br.com.cwi.nespresso_app.presentation.base.BaseViewModel

class AccessoriesViewModel  : BaseViewModel() {

    private val _accessories = MutableLiveData<List<Type>>()
    val accessories: LiveData<List<Type>> = _accessories

    private val repository: CoffeeRepository = CoffeeRepositoryImpl(
        CategoryCoffeMapper(), MachineMapper(), CategoryAccessoriesMapper()
    )

    fun fetchAccessories() {
        launch {
            val categoryList = repository.getAccessories()
            _accessories.postValue(getAccessoriesType(categoryList))
        }
    }

    private fun getAccessoriesType(categoryAccessoriesList: List<CategoryAccessories>): List<Type> {
        val accessorieList = mutableListOf<Type>()

        categoryAccessoriesList.forEach {
            accessorieList.add(it)
            accessorieList.addAll(it.accessories)
        }
        return accessorieList
    }

}