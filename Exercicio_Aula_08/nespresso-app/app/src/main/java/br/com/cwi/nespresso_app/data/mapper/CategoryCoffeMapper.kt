package br.com.cwi.nespresso_app.data.mapper

import br.com.cwi.nespresso_app.data.entity.CategoryCoffeResponse
import br.com.cwi.nespresso_app.domain.entity.CategoryCoffe

class CategoryCoffeMapper: DomainMapper<CategoryCoffeResponse, CategoryCoffe> {
    override fun toDomain(from: CategoryCoffeResponse) = CategoryCoffe(
        category = from.category,
        coffees = CoffeeMapper().toDomain(from.coffees)
    )

    override fun toDomain(from: List<CategoryCoffeResponse>) = from.map {
        toDomain(it)
    }

}