package br.com.cwi.nespresso_app.data.entity

sealed class MachineType(
    val type: Int
)

