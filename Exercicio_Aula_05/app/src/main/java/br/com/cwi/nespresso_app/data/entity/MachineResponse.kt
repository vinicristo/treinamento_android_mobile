package br.com.cwi.nespresso_app.data.entity

import com.squareup.moshi.Json

data class MachineResponse (
    @Json(name = "id") val id: Int,
    @Json(name = "nome") val name: String,
    @Json(name = "preco") val price: Double,
    @Json(name = "descricao") val description: String,
    @Json(name = "intensidade") val intensity: Int?,
    @Json(name = "imagem") val urlImage: String,
): MachineType(type = 0)