package br.com.cwi.nespresso_app.presentation.products.machine

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.cwi.nespresso_app.R
import br.com.cwi.nespresso_app.base.toMoneyFormat
import br.com.cwi.nespresso_app.data.entity.CoffeeResponse
import br.com.cwi.nespresso_app.data.entity.MachineResponse
import br.com.cwi.nespresso_app.data.entity.MachineType
import br.com.cwi.nespresso_app.databinding.ItemCoffeBinding
import br.com.cwi.nespresso_app.databinding.ItemMachineBinding


class MachinesAdapter(private val list: List<MachineType>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return MachineViewHolder(inflateView(R.layout.item_machine, parent))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position]

        (holder as MachineViewHolder).bind(item as MachineResponse)

    }

    override fun getItemCount() = list.size

    override fun getItemViewType(position: Int) = list[position].type

    private fun inflateView(layout: Int, parent: ViewGroup) = LayoutInflater
        .from(parent.context)
        .inflate(layout, parent, false)
}

class MachineViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    private val tvTitle = ItemMachineBinding.bind(item).tvMachineTitle
    private val ivImage = ItemMachineBinding.bind(item).ivMachineImage
    private val tvPrice = ItemMachineBinding.bind(item).tvMachinePrice


    fun bind(item: MachineResponse) {
        tvTitle.text = item.name
      //ivImage.text = item.urlImage
        tvPrice.text = item.price.toMoneyFormat()
    }
}